#!/bin/bash
#
# Description	: Android Build Script.
# Authors		: jianjun jiang - jerryjianjun@gmail.com
# Version		: 2.00
# Notes			: None
#

#
# JAVA PATH
#
export PATH=/usr/lib/jvm/java-8-openjdk-amd64/bin:$PATH

#
# Some Directories
#
BS_DIR_TOP=$(cd `dirname $0` ; pwd)
BS_DIR_TOOLS=${BS_DIR_TOP}/tools
BS_DIR_RELEASE=${BS_DIR_TOP}/out/release
BS_DIR_TARGET=${BS_DIR_TOP}/out/target/product/x3399/
BS_DIR_UBOOT=${BS_DIR_TOP}/u-boot
BS_DIR_KERNEL=${BS_DIR_TOP}/kernel

#
# Target Config
#
BS_CONFIG_BOOTLOADER_UBOOT=x3399_defconfig
BS_CONFIG_KERNEL=x3399_defconfig
BS_CONFIG_KERNEL_DTB=x3399-development-board.img
BS_CONFIG_FILESYSTEM=PRODUCT-x3399-userdebug

setup_environment()
{
	LANG=C
	PATH=${BS_DIR_TOP}/out/host/linux-x86/bin:$PATH;
	cd ${BS_DIR_TOP};
	mkdir -p ${BS_DIR_RELEASE} || return 1
}

build_bootloader_uboot()
{
	# Compiler uboot
	cd ${BS_DIR_UBOOT} || return 1
	make ARCHV=aarch64 distclean || return 1
	make ARCHV=aarch64 ${BS_CONFIG_BOOTLOADER_UBOOT} || return 1
	make ARCHV=aarch64 -j${threads} || return 1

	# Copy bootloader to release directory
	cp -v ${BS_DIR_UBOOT}/trust.img ${BS_DIR_RELEASE}
	cp -v ${BS_DIR_UBOOT}/rk3399_loader_*.bin ${BS_DIR_RELEASE}
	cp -v ${BS_DIR_UBOOT}/uboot.img ${BS_DIR_RELEASE}

	return 0
}

build_kernel()
{
	# Compiler kernel
	cd ${BS_DIR_KERNEL} || return 1
	make ARCH=arm64 ${BS_CONFIG_KERNEL} || return 1
	make -j${threads} ARCH=arm64 Image || return 1
	make -j${threads} ARCH=arm64 ${BS_CONFIG_KERNEL_DTB} || return 1

	# Copy kernel to release directory
	cp -v ${BS_DIR_KERNEL}/resource.img ${BS_DIR_RELEASE}
	cp -v ${BS_DIR_KERNEL}/kernel.img ${BS_DIR_RELEASE}

	return 0
}

build_system()
{
	cd ${BS_DIR_TOP} || return 1
	source build/envsetup.sh || return 1
	make -j${threads} ${BS_CONFIG_FILESYSTEM} || return 1

	echo "create boot.img..."
	[ -d ${BS_DIR_TARGET}/root ] && \
	mkbootfs ${BS_DIR_TARGET}/root | minigzip > ${BS_DIR_TARGET}/ramdisk.img && \
	truncate -s "%4" ${BS_DIR_TARGET}/ramdisk.img && \
	${BS_DIR_TOOLS}/mkkrnlimg ${BS_DIR_TARGET}/ramdisk.img ${BS_DIR_RELEASE}/boot.img >/dev/null

	echo "create recovery.img..."
	[ -d ${BS_DIR_TARGET}/recovery/root ] && \
	mkbootfs ${BS_DIR_TARGET}/recovery/root | minigzip > ${BS_DIR_TARGET}/ramdisk-recovery.img && \
	truncate -s "%4" ${BS_DIR_TARGET}/ramdisk-recovery.img && \
	mkbootimg --kernel ${BS_DIR_TARGET}/kernel --ramdisk ${BS_DIR_TARGET}/ramdisk-recovery.img --output ${BS_DIR_TARGET}/recovery.img && \
	cp -av ${BS_DIR_TARGET}/recovery.img ${BS_DIR_RELEASE}

	echo "create system.img..."
	python ${BS_DIR_TOP}/build/tools/releasetools/build_image.py \
		${BS_DIR_TARGET}/system ${BS_DIR_TARGET}/obj/PACKAGING/systemimage_intermediates/system_image_info.txt \
		${BS_DIR_TARGET}/system.img ${BS_DIR_TARGET}/system
	echo "translate verified sparse image to raw image... "
	simg2img ${BS_DIR_TARGET}/system.img ${BS_DIR_RELEASE}/system.img

#	system_size=1610612736
#	[ ${system_size} -gt "0" ] || { echo "Please build android first!!!" && exit 1; }
#	MAKE_EXT4FS_ARGS=" -L system -S ${BS_DIR_TARGET}/root/file_contexts -a system ${BS_DIR_RELEASE}/system.img ${BS_DIR_TARGET}/system"
#	ok=0
#	while [ "$ok" = "0" ]; do
#		make_ext4fs -l ${system_size} ${MAKE_EXT4FS_ARGS} >/dev/null 2>&1 &&
#		tune2fs -c -1 -i 0 ${BS_DIR_RELEASE}/system.img >/dev/null 2>&1 &&
#		ok=1 || system_size=$((${system_size} + 5242880))
#	done
#	e2fsck -fyD ${BS_DIR_RELEASE}/system.img >/dev/null 2>&1 || true

	return 0
}

build_update()
{
	cd ${BS_DIR_RELEASE} || return 1
	
	# Make update-android.img
	echo "create update-android.img..."
	cp -av ${BS_DIR_TOOLS}/package-file ${BS_DIR_RELEASE}/package-file || return 1;
	${BS_DIR_TOOLS}/afptool -pack ${BS_DIR_RELEASE}/ ${BS_DIR_RELEASE}/temp.img || return 1;
	${BS_DIR_TOOLS}/rkImageMaker -RK330C ${BS_DIR_RELEASE}/rk3399_loader_v1.12.109.bin ${BS_DIR_RELEASE}/temp.img ${BS_DIR_RELEASE}/update-android.img -os_type:androidos || return 1;
	rm -fr ${BS_DIR_RELEASE}/temp.img || return 1;

	return 0
}

copy_other_files()
{
	cd ${BS_DIR_TOP} || return 1

	cp -av ${BS_DIR_TOP}/device/rockchip/rk3399/x3399/parameter.txt ${BS_DIR_RELEASE} || return 1;
	cp -av ${BS_DIR_TOP}/device/rockchip/rk3399/x3399/misc.img ${BS_DIR_RELEASE} || return 1;
	return 0
}

threads=1
uboot=no
kernel=no
system=no
update=no

if [ -z $1 ]; then
	uboot=yes
	kernel=yes
	system=yes
	update=yes
fi

while [ "$1" ]; do
    case "$1" in
	-j=*)
		x=$1
		threads=${x#-j=}
		;;
	-u|--uboot)
		uboot=yes
	    ;;
	-k|--kernel)
	    kernel=yes
	    ;;
	-s|--system)
		system=yes
	    ;;
	-U|--update)
		update=yes
	    ;;
	-a|--all)
		uboot=yes
		kernel=yes
		system=yes
		update=yes
	    ;;
	-h|--help)
	    cat >&2 <<EOF
Usage: build.sh [OPTION]
Build script for compile the source of telechips project.

  -j=n                 using n threads when building source project (example: -j=16)
  -u, --uboot          build bootloader uboot from source
  -k, --kernel         build kernel from source
  -s, --system         build android file system from source
  -U, --update         build update file
  -a, --all            build all, include anything
  -h, --help           display this help and exit
EOF
	    exit 0
	    ;;
	*)
	    echo "build.sh: Unrecognised option $1" >&2
	    exit 1
	    ;;
    esac
    shift
done

setup_environment || exit 1
copy_other_files || exit 1

if [ "${uboot}" = yes ]; then
	build_bootloader_uboot || exit 1
fi

if [ "${kernel}" = yes ]; then
	build_kernel || exit 1
fi

if [ "${system}" = yes ]; then
	build_system || exit 1
fi

if [ "${update}" = yes ]; then
	build_update || exit 1
fi

exit 0
